# menubar.widget

Menubar widget for Übersicht. Includes:

* Currently running application (with title if available).
* Today's time.
* The current time.
* Battery percentage (icon to indicate if we are on AC power).
* WiFi SSID or "no connection" message.
* Spotify's status - indicates if Spotify is not running, if it is paused or the current playing artist and track.
* Applications are moved on each run so they don't block the menubar, this can be disabled by commenting out `resizeWindows()` in `menubar.go`.

## Installation

1. To compile the application for use in the widget, you will need Go [installed and configured](https://golang.org/doc/install).
2. Next, go get the source for the menubar.widget by running `go get gitlab.com/nylar/menubar.widget`, which will store the package at `$GOPATH/src/gitlab.com/nylar/menubar.widget`.
3. `cd` to `$GOPATH/src/gitlab.com/nylar/menubar.widget` and compile the application by running `go build -o "menubar"`.
4. `cd` to your Übersicht widgets directory, and create symlink to the application folder with `ln -s $GOPATH/src/gitlab.com/nylar/menubar.widget ./menubar.widget`.
5. Assuming Übersicht is running, you should see the menubar at the top of your screen. To hide Apple's menubar, you will need El Capitan (10.11.x) installed then follow these [instructions](http://www.cnet.com/uk/how-to/how-to-hide-the-menu-bar-in-os-x-el-capitan/).

## Updating

1. Update the application with `go get -u gitlab.com/nylar/menubar.widget`.
2. `cd` into `$GOPATH/src/gitlab.com/nylar/menubar.widget`.
3. Rebuild the application with `go build -o "menubar"`.
4. Übersicht should run the updated code automatically on the next refresh cycle, if this isn't the case, you can refresh all widgets by Übersicht > Refresh all Widgets. 

## Menubar preview

![Menubar preview](screenshots/menubar.png)

## Desktop preview

![Desktop preview](screenshots/fullscreen.png)

## Currently running app + title preview

![Currently running app and title preview](screenshots/current-running-app-plus-title.png)

## Mini-widgets preview

AC power is plugged in and there is an active WiFi connection.

![Mini-widgets preview](screenshots/date-time-battery_status-wifi_ssid.png)

## Spotify

#### Paused

![Spotify paused](screenshots/spotify-status.png)

#### Now playing

![Spotify currently playing](screenshots/spotify-now-playing.png)
