command: './menubar.widget/menubar'

refreshFrequency: 1500

style: """
    background: rgba(16,29,24,0.9)
    top: 0;
    left: 0;
    height: 22px;
    width: 100%;
    color: white;
    font-family: 'Menlo';
    font-size: 10px;
    padding-top: 1px;

    table
        table-layout: fixed;
        width: 100%;

    td.focused
        color: #EE829F;
        width: 30%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-left: 5px;

    td.date
        color: #C0B7F9;
        text-align: center;

    td.battery
        color: #FFEFCC;
        text-align: center;

    td.time
        color: #97BBF7;
        text-align: center;

    td.wifi
        color: #97E0FF;
        text-align: center;

    span.inactive
        opacity: 0.4;

    td.spotify
        color: #A5FDE1;
        width: 30%;
        white-space: nowrap;
        overflow: hidden;
        text-align: right;
        text-overflow: ellipsis;
        padding-right: 5px;
"""

render: (output) -> """
<table>
    <tbody id="data">
    </tbody>
</table>
"""

renderInfo: (focusedWindow, date, time, battery, charging, wifi, spotify) -> """
    <tr>
        <td class="focused">#{focusedWindow}</td>
        <td></td>
        <td class="date">#{date}</td>
        <td class="time">#{time}</td>
        <td class="battery">#{charging}#{battery}</td>
        <td class="wifi">#{wifi}</td>
        <td></td>
        <td class="spotify">#{spotify}</td>
    </tr>
"""

update: (output, dom) ->
    $(dom).find('#data').html ''

    data = JSON.parse(output)

    console.log data.wifi.ssid

    if data.focused_window.title == ""
        focusedWindow = data.focused_window.app
    else
        focusedWindow = "#{data.focused_window.app} | #{data.focused_window.title}"

    date = data.date
    time = data.time
    battery = data.battery

    charging = ""
    if data.power_source == "\'AC Power\'"
        charging = "⚡ "

    if data.wifi.ssid == "SSID:"
        wifiStatus = "<span class=\"inactive\">No Connection</span>"
    else
        wifiStatus = data.wifi.ssid

    if data.spotify.running
        if data.spotify.status == "paused"
            spotify = "Spotify | paused"
        else
            spotify = "Spotify | #{data.spotify.track} - #{data.spotify.artist}"
    else
        spotify = "Spotify | not running"

    $(dom).find('#data').append @renderInfo(
        focusedWindow,
        date,
        time,
        battery,
        charging,
        wifiStatus,
        spotify,
    )
