package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"sync"
	"time"
)

type menuBar struct {
	Date          string        `json:"date"`
	Time          string        `json:"time"`
	Battery       string        `json:"battery"`
	PowerSource   string        `json:"power_source"`
	WiFi          wifi          `json:"wifi"`
	FocusedWindow focusedWindow `json:"focused_window"`
	Spotify       spotify       `json:"spotify"`
}

type wifi struct {
	SSID string `json:"ssid"`
}

type focusedWindow struct {
	Application string `json:"app"`
	Title       string `json:"title"`
}

type spotify struct {
	Running bool   `json:"running"`
	Status  string `json:"status"`
	Artist  string `json:"artist"`
	Track   string `json:"track"`
}

func (s spotify) isRunning() bool {
	spotifyRunningCmd := `ps -ef | grep 'MacOS/Spotify$' | grep -v "grep" | wc -l`

	output, err := exec.Command("bash", "-c", spotifyRunningCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return false
	}

	if string(bytes.TrimSpace(output)) == "1" {
		return true
	}

	return false
}

func (mb *menuBar) date() {
	now := time.Now()
	weekDay := now.Weekday().String()
	month := now.Month().String()

	mb.Date = fmt.Sprintf("%s %02d %s", weekDay[:3], now.Day(), month[:3])
}

func (mb *menuBar) time() {
	now := time.Now()

	mb.Time = fmt.Sprintf("%02d:%02d", now.Hour(), now.Minute())
}

func (mb *menuBar) ssid() {
	ssidCmd := `/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk '/ SSID/ {print substr($0, index($0, $2))}'`

	output, err := exec.Command("bash", "-c", ssidCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.WiFi.SSID = string(bytes.TrimSpace(output))
}

func (mb *menuBar) battery() {
	batteryCmd := `pmset -g batt | egrep "([0-9]+\%).*" -o --colour=auto | cut -f1 -d';'`

	output, err := exec.Command("bash", "-c", batteryCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.Battery = string(bytes.TrimSpace(output))
}

func (mb *menuBar) powerSource() {
	powerSourceCmd := `pmset -g batt | egrep "(?:^|\s)'([^']*?)'(?:$|\s)" -o --colour=auto`

	output, err := exec.Command("bash", "-c", powerSourceCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.PowerSource = string(bytes.TrimSpace(output))
}

func (mb *menuBar) focusedApp() {
	focusedAppCmd := `osascript -e 'tell application "System Events"
       set frontApp to name of first application process whose frontmost is true
       return frontApp
    end tell'`

	output, err := exec.Command("bash", "-c", focusedAppCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.FocusedWindow.Application = string(bytes.TrimSpace(output))
}

func (mb *menuBar) focusedTitle() {
	focusedTitleCmd := `osascript -e 'tell application "System Events"
       set frontApp to name of first application process whose frontmost is true
        tell process frontApp
            try
                tell (1st window whose value of attribute "AXMain" is true)
                    set windowTitle to value of attribute "AXTitle"
                    return windowTitle
                end tell
            end try
        end tell
    end tell'`

	output, err := exec.Command("bash", "-c", focusedTitleCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.FocusedWindow.Title = string(bytes.TrimSpace(output))
}

func (mb *menuBar) spotifyRunning() {
	if mb.Spotify.isRunning() {
		mb.Spotify.Running = true
	}
}

func (mb *menuBar) spotifyStatus() {
	spotifyStatusCmd := `osascript -e 'if application "Spotify" is running then
        tell application "Spotify" to player state as string
    end if'`

	output, err := exec.Command("bash", "-c", spotifyStatusCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.Spotify.Status = string(bytes.TrimSpace(output))
}

func (mb *menuBar) spotifyArtist() {
	spotifyArtistCmd := `osascript -e 'if application "Spotify" is running then
        tell application "Spotify" to artist of current track as string
    end if'`

	output, err := exec.Command("bash", "-c", spotifyArtistCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.Spotify.Artist = string(bytes.TrimSpace(output))
}

func (mb *menuBar) spotifyTrack() {
	spotifyTrackCmd := `osascript -e 'if application "Spotify" is running then
        tell application "Spotify" to name of current track as string
    end if'`

	output, err := exec.Command("bash", "-c", spotifyTrackCmd).Output()
	if err != nil {
		log.Println(err.Error())
		return
	}
	mb.Spotify.Track = string(bytes.TrimSpace(output))
}

func resizeWindows() {
	// TODO: Weird behaviour for child elements of an application, they shouldn't be moved.
	resizeWindowsCmd := `osascript -e 'property excludedApplicationNames : {"iTerm2", "Übersicht"}
    tell application "System Events" to get the name of every process whose visible is true
    repeat with theProcess in the result
        set theProcessName to theProcess as string
        if theProcessName is not in excludedApplicationNames then
            tell application "System Events" to set position of windows of process theProcess to {0, 24}
        end if
    end repeat'`

	if err := exec.Command("bash", "-c", resizeWindowsCmd).Run(); err != nil {
		log.Println(err.Error())
		return
	}
}

func main() {
	mb := &menuBar{}

	// Run each command concurrently, the number in wg.Add should be the number
	// of commands we plan to run concurrently.
	var wg sync.WaitGroup
	wg.Add(12)

	go func() {
		defer wg.Done()
		mb.date()
	}()
	go func() {
		defer wg.Done()
		mb.time()
	}()

	// WiFi information
	go func() {
		defer wg.Done()
		mb.ssid()
	}()

	// Battery information
	go func() {
		defer wg.Done()
		mb.battery()
	}()
	go func() {
		defer wg.Done()
		mb.powerSource()
	}()

	// Focused window information
	go func() {
		defer wg.Done()
		mb.focusedApp()
	}()
	go func() {
		defer wg.Done()
		mb.focusedTitle()
	}()

	// Spotify information
	go func() {
		defer wg.Done()
		mb.spotifyRunning()
	}()
	go func() {
		defer wg.Done()
		mb.spotifyStatus()
	}()
	go func() {
		defer wg.Done()
		mb.spotifyArtist()
	}()
	go func() {
		defer wg.Done()
		mb.spotifyTrack()
	}()

	// Move any windows out of the way of the menubar
	go func() {
		defer wg.Done()
		// resizeWindows()
	}()

	wg.Wait()

	json.NewEncoder(os.Stdout).Encode(mb)
}
